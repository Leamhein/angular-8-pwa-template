import { SharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		SharedModule,
	],
	declarations: [
	],
	exports: [],
})
export class ComponentsModule {}
